package com.java;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.java.model.Person;

public class Bootstrap {
	final static Logger LOG = Logger.getLogger(Bootstrap.class);

	public static void main(String[] args) {
		LOG.debug("debug log before setting info");
		LOG.trace("trace log before setting info");
		LOG.setLevel(Level.INFO);
		LOG.debug("after gebug log");
		LOG.trace("trace log after setting info");
		if(LOG.isDebugEnabled()) {
			LOG.info("DEBUG enabled");
		}
		else
		{
			LOG.info("Debug is disabled");
		}
		Person p=new Person();
		p.setId(123L);
		p.setName("Lara");
		LOG.debug("hello world debug log"+p);
		

	}

}
